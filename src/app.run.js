/**
 * @author Diego <Diego@eharvesthub.com>
 */
(function () {
  'use strict';
  angular
    .module('app')
    .run(runConfig);

  /** @ngInject */
  function runConfig($rootScope, Restangular, $cookies) {
    $rootScope.globals = $cookies.getObject('globals') || {};
    if ($rootScope.globals.currentUser) {
      Restangular.setDefaultHeaders({
        'x-access-token': $cookies.getObject('globals').currentUser.token
      });
    }
  }
})();
