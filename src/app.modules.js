(function () {
  angular
    .module('app', [
      'app.core',
      'app.routes',

      'app.footer',
      'app.header',
      'app.signin'

    ]);
})();
