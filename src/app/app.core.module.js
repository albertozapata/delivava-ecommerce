/**
 * @author Diego <Diego@eharvesthub.com>
 */
(function () {
  'use strict';
  angular
    .module('app.core', [
      'ui.router',
      'restangular',
      'ngAnimate',
      'ngCookies',
      'ngMessages',
      'ngLodash',
      'ngSanitize',
      'app.util'
    ]);
})();
