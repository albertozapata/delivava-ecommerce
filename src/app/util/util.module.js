/**
 * @author Diego <Diego@eharvesthub.com>
 */
(function () {
  'use strict';
  angular
    .module('app.util', []);
})();
