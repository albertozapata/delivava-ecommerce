(function () {
  'use strict';
  angular
    .module('app.routes.signin', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    const signIn = {
      name: 'signin',
      url: '/signin',
      views: {
        content: {
          templateUrl: '/app/signin/views/signin.template.html',
          controller: 'SigninController',
          controllerAs: 'signinCtrl'
        }
      }

    };

    $stateProvider.state(signIn);
  }
})();
