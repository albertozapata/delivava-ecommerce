/**
 * @author Diego <Diego@eharvesthub.com>
 */
(function () {
  'use strict';
  angular
    .module('app.signin')
    .controller('SigninController', SigninController);

  /** @ngInject */
  function SigninController() {
    const vm = this;

    vm.prueba = 'hola mundo';
  }
})();
