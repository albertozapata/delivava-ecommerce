/**
 * @author Diego <Diego@eharvesthub.com>
 */
(function () {
  'use strict';
  angular
    .module('app')
    .config(config);

  /** @ngInject */
  function config($urlRouterProvider, $locationProvider, RestangularProvider, API) {
    $locationProvider.html5Mode(true).hashPrefix('!');
    $urlRouterProvider.otherwise('/');
    RestangularProvider.setBaseUrl(API.URL);
  }
})();
