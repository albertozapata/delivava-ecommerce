'use strict';
const path = require('path');

const gulp = require('gulp');
const del = require('del');
const filter = require('gulp-filter');
const gulpNgConstants = require('gulp-ng-constant');
const rename = require('gulp-rename');
const conf = require('../conf/gulp.conf');
const gutil = require('gulp-util');

gulp.task('clean', clean);
gulp.task('other', other);
gulp.task('angular-constants', ngConstant);

function clean() {
  return del([conf.paths.dist, conf.paths.tmp]);
}

function other() {
  const fileFilter = filter(file => file.stat.isFile());

  return gulp.src([
    path.join(conf.paths.src, '/**/*'),
    path.join(`!${conf.paths.src}`, '/**/*.{scss,js,html}')
  ])
    .pipe(fileFilter)
    .pipe(gulp.dest(conf.paths.dist));
}

function ngConstant() {
  const apiUrl = process.env.API_URL;
  if (!apiUrl) {
    gutil.log(gutil.colors.red('Please provide an API_URL Environment Variable'));
    process.exit();
    return Promise.resolve();
  }
  const constants = {
    API: {
      URL: apiUrl
    }
  };
  return gulpNgConstants({
    name: 'app',
    dist: 'app.constants.js',
    templatePath: './conf/constant.template.ejs',
    wrap: false,
    constants,
    stream: true
  })
    .pipe(rename('app.constants.js'))
    .pipe(gulp.dest(conf.paths.src));
}
